// ES6
class Animal {
    constructor(name) {
        this.name = name;
    }
    die() {
        console.log(`${this.name} died`);
    }
}

// ES5
function Animal(name){
    this.name = name;
}
Animal.prototype.die = function(){
    console.log(`${this.name} died`);
}


let snoop = new Animal("Snoop");
let leo = new Animal("Leo");