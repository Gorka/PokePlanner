"use strict"

let myURL = "https://pokeapi.co/api/v2/pokemon/";
let pokemonListUrl ="https://pokeapi.co/api/v2/generation/1";
let allTypesURL = "https://pokeapi.co/api/v2/type";

let pokemonList = [];
let teamList = [];
let typesList = [];

let inputElement = document.getElementById("input1");
let imageElement = document.getElementById("image1");
let teamArticle  = document.querySelector("#teamArticle");
let typesArticle = document.querySelector("#typesArticle");
let type1Element = document.getElementById("type1");
let type2Element = document.getElementById("type2");
let myDatalist = document.getElementById("pokemon-datalist");

fetch(pokemonListUrl)
.then((response)=> response.json())
.then((data)=> createPokemonList(data.pokemon_species));

function createPokemonList(data){
    for (const iterator of data) {
        pokemonList.push(iterator.name)
        let thisOption = document.createElement("option");
        thisOption.setAttribute("value", iterator.name);
        myDatalist.appendChild(thisOption);
    }
}

fetch(allTypesURL)
.then((response)=> response.json())
.then((data)=> createTypesList(data) );

function createTypesList(data){
    for(let i = 0; i<16; i++){
        typesList[i] = data.results[i].name;
    }
}

function Pokemon(name, types){
    this.name = name;
    this.types = types;
}
for (let i = 0; i < 6; i++) {
    createTeamSlot(i);
}

function createTeamSlot(i){
    let teamSlot = document.createElement("article");
    teamSlot.innerHTML=`
    <form>
        <input type="text" list ="pokemon-datalist" id="input${i}" oninput="onChange(${i})">
    </form>
    <img src="" id="image${i}" >
    <p id ="typeA${i}"></p> 
    <p id ="typeB${i}"></p> 
    `
    teamArticle.appendChild(teamSlot);
}

function onChange(inputID){
    let inputedWord = document.getElementById("input"+inputID).value;
    for (const iterator of pokemonList) {
        if(inputedWord == iterator){
            return myFetch(inputedWord, inputID);
        }
    }
}
function myFetch(search, inputID){
fetch(myURL+search)
.then((response)=> response.json())
.then((data)=>showPoke(data, inputID));
}
function showPoke(data, inputID){
    let imageElement = document.getElementById("image"+inputID);
    let typeAElement = document.getElementById("typeA"+inputID);
    let typeBElement = document.getElementById("typeB"+inputID);

    let myPokemonTypes = getTypes(data);
    teamList[inputID] = (new Pokemon(data.name, myPokemonTypes));
    //console.log(teamArray);

    typeAElement.innerHTML = teamList[inputID].types[0];
    if (data.types.length > 1){
        typeBElement.innerHTML = teamList[inputID].types[1];
    } else
        typeBElement.innerHTML = "";
    imageElement.src = data.sprites.front_default;
}

function getTypes(data){
    let myPokemonTypes = [];
    myPokemonTypes.push(data.types[0].type.name)
    if (data.types.length > 1){
        myPokemonTypes.push(data.types[1].type.name)
    } 
    return myPokemonTypes;
}